// Variables to keep track of game scores, counts, choices, and mode
let user1Score = 0;
let user2Score = 0;
let drawCount = 0;
let gameCount = 0;
let maxGames = 0;
let choices = ['', '']; // Array to store player choices
let mode = ''; // Variable to store the game mode

// Function to start the game
function startGame() {
    // Initialize game variables
    maxGames = parseInt(document.getElementById('game-count').value);
    gameCount = 0;
    user1Score = 0;
    user2Score = 0;
    drawCount = 0;
    mode = '';
    
    // Update UI
    updateStatistics();
    document.getElementById('game-count-selection').classList.add('hidden');
    document.getElementById('mode-selection').classList.remove('hidden');
    document.getElementById('player-2-choices').classList.add('hidden');
}

// Function to select the game mode
function selectMode(selectedMode) {
    mode = selectedMode; // Set the game mode

    // Update UI based on selected mode
    const modeSelection = document.getElementById('mode-selection');
    const pcModeSelection = document.getElementById('pc-mode-selection');
    if (mode === 'player-vs-pc') {
        // Hide mode selection and show player 1 choices
        if (modeSelection) modeSelection.classList.add('hidden');
        if (pcModeSelection) pcModeSelection.classList.add('hidden');
        document.getElementById('player-1-choices').classList.remove('hidden');
        document.getElementById('player-2-choices').classList.add('hidden');
        document.getElementById('score').classList.remove('hidden');
        document.getElementById('statistics').classList.remove('hidden');
        enableGameButtons();
        document.getElementById('random').checked = true; // Set PC mode radio button to "random"
    } else if (mode === 'player-vs-player') {
        // Hide mode selection and show player 1 choices
        if (modeSelection) modeSelection.classList.add('hidden');
        if (pcModeSelection) pcModeSelection.classList.add('hidden');
        document.getElementById('player-1-choices').classList.remove('hidden');
        document.getElementById('player-2-choices').classList.add('hidden');
        document.getElementById('score').classList.remove('hidden');
        document.getElementById('statistics').classList.remove('hidden');
    }
}

// Function to handle window onload event
window.onload = function() {
    // Hide player 1 choices and PC mode selection
    const player1Choices = document.getElementById('player-1-choices');
    if (player1Choices) player1Choices.classList.add('hidden');
    const pcModeSelection = document.getElementById('pc-mode-selection');
    if (pcModeSelection) pcModeSelection.classList.add('hidden');
};

// Function to handle player vs PC mode selection
function selectPCMode() {
    // Hide PC mode selection and show player 1 choices
    document.getElementById('pc-mode-selection').classList.add('hidden');
    document.getElementById('player-1-choices').classList.remove('hidden');
}

// Function to play the game
function playGame(choice, player) {
    console.log("playGame function called with choice:", choice, "and player:", player);

    // Check if max games reached
    if (gameCount >= maxGames) {
        return; // Stop playing if max games reached
    }

    // Check game mode and call appropriate function
    if (mode === 'player-vs-player') {
        playPvPGame(choice, player);
    } else if (mode === 'player-vs-pc') {
        playPvCGame(choice);
    }
}

// Function to play player vs player game
function playPvPGame(choice, player) {
    choices[player - 1] = choice; // Store player choice

    if (player === 1) {
        // Show player 2 choices
        document.getElementById('player-1-choices').classList.add('hidden');
        document.getElementById('player-2-choices').classList.remove('hidden');
    } else if (player === 2) {
        // Determine winner and show player 1 choices
        determineWinnerPvP(choices[0], choices[1]);
        document.getElementById('player-2-choices').classList.add('hidden');
        document.getElementById('player-1-choices').classList.remove('hidden');
        gameCount++;
        if (gameCount >= maxGames) {
            showStatistics();
            disableGameButtons();
            return;
        }
    }
}

// Function to play player vs PC game
function playPvCGame(choice) {
    choices[0] = choice; // Store player choice
    const pcChoice = getPCChoicePlayerVsPC(); // Get PC choice
    determineWinnerPlayerVsPC(choices[0], pcChoice); // Determine winner
    gameCount++;
    if (gameCount >= maxGames) {
        showStatistics();
        disableGameButtons();
        return;
    }
}

// Function to get random PC choice
function getPCChoicePlayerVsPC() {
    return getRandomChoice();
}

// Function to get a random choice
function getRandomChoice() {
    const choices = ['kámen', 'nůžky', 'papír', 'tapír', 'spock'];
    return choices[Math.floor(Math.random() * choices.length)];
}

// Function to get smart PC choice
function getSmartPCChoice() {
    // Implement smart PC choice logic here
}

// Function to determine winning choice for PC
function getWinningChoice(playerChoice) {
    // Implement winning choice logic here
}

// Function to determine winner in player vs player game
function determineWinnerPvP(choice1, choice2) {
    // Determine winner based on choices
    if (choice1 === choice2) {
        drawCount++;
        document.getElementById('result').innerText = `Oba hráči vybrali ${choice1}. Remíza!`;
    } else {
        const winTable = {
            'kámen': ['nůžky', 'tapír'],
            'nůžky': ['papír', 'tapír'],
            'papír': ['kámen', 'spock'],
            'tapír': ['spock', 'papír'],
            'spock': ['kámen', 'nůžky']
        };

        if (winTable[choice1][0] === choice2) {
            user1Score++;
            document.getElementById('result').innerText = `Hráč 1 vyhrál!`;
        } else {
            user2Score++;
            document.getElementById('result').innerText = `Hráč 2 vyhrál!`;
        }
    }
    updateStatistics();
}

// Function to determine winner in player vs PC game
function determineWinnerPlayerVsPC(choice1, choice2) {
    // Determine winner based on choices
    if (choice1 === choice2) {
        drawCount++;
        document.getElementById('result').innerText = `Oba hráči vybrali ${choice1}. Remíza!`;
    } else {
        const winTable = {
            'kámen': ['nůžky', 'tapír'],
            'nůžky': ['papír', 'papír'],          
            'papír': ['kámen', 'spock'],
            'tapír': ['spock', 'papír'],
            'spock': ['kámen', 'nůžky']
        };

        if (winTable[choice1][0] === choice2) {
            user1Score++;
            document.getElementById('result').innerText = `Hráč vyhrál!`;
        } else {
            user2Score++;
            document.getElementById('result').innerText = `PC vyhrál!`;
        }
    }
    updateStatistics();
}

// Function to update game statistics
function updateStatistics() {
    // Update game statistics displayed on UI
    document.getElementById('player1-wins').innerText = `Hráč 1 vyhrál: ${user1Score}`;
    if (mode === 'player-vs-player') {
        document.getElementById('player2-wins').innerText = `Hráč 2 vyhrál: ${user2Score}`;
    } else if (mode === 'player-vs-pc') {
        document.getElementById('player2-wins').innerText = `PC vyhrál: ${user2Score}`;
    }
    document.getElementById('ties').innerText = `Remízy: ${drawCount}`;
}

// Function to show final game statistics
function showStatistics() {
    // Show final game statistics on UI
    document.getElementById('statistics').classList.remove('hidden');
    if (user1Score > user2Score) {
        document.getElementById('result').innerText = `Hráč 1 vyhrál po ${maxGames} hrách!`;
    } else if (user1Score < user2Score) {
        if (mode === 'player-vs-player') {
            document.getElementById('result').innerText = `Hráč 2 vyhrál po ${maxGames} hrách!`;
        } else if (mode === 'player-vs-pc') {
            document.getElementById('result').innerText = `PC vyhrál po ${maxGames} hrách!`;
        }
    } else {
        document.getElementById('result').innerText = `Remíza po ${maxGames} hrách!`;
    }

    disableGameButtons();
}

// Function to disable game buttons
function disableGameButtons() {
    // Disable game buttons
    const buttons = document.querySelectorAll('.choice');
    buttons.forEach(button => {
        button.disabled = true;
    });
}

// Function to enable game buttons
function enableGameButtons() {
    // Enable game buttons
    const buttons = document.querySelectorAll('.choice');
    buttons.forEach(button => {
        button.disabled = false;
    });
}

